+++
title = "Join the Veloren community!"
description = "Join the Veloren community!"

weight = 0
+++

Veloren is an open-source, community-driven project! Join us in making the game the best it can be.

## News

* [This Week In Veloren — Weekly dev blog](https://veloren.net/devblogs/)
* [Mastodon — Keep up to date with the project](https://floss.social/@veloren)
* [Twitter — Keep up to date with the project](https://twitter.com/velorenproject/)

## Discussion & Development

* [Discord — Talk to the community and developers](https://discord.gg/veloren-community-449602562165833758)
* [Matrix — Talk to the community and developers](https://matrix.to/#/#veloren-space:fachschaften.org) | [Room List (Old)](@/matrix_room_list.md) 

## Community

* [Reddit — Discuss Veloren](https://www.reddit.com/r/Veloren/)
* [YouTube — Watch videos about development news](https://youtube.com/@Veloren) | [(RSS)](https://www.youtube.com/feeds/videos.xml?channel_id=UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious)](https://yewtu.be/channel/UCmRjlnKnSRRihWPPNasl_Qw) | [(Invidious - RSS)](https://yewtu.be/feed/channel/UCmRjlnKnSRRihWPPNasl_Qw)
* [Twitch — Core development channel](https://www.twitch.tv/veloren_dev) | [Veloren streamers live on Twitch](https://www.twitch.tv/directory/game/Veloren)

## Wiki & Information

* [The wiki — Contribute to or read articles about playing the game](https://wiki.veloren.net/)
* [The book — A technical manual for players and developers](https://book.veloren.net/)

## Source Code

* [GitLab — Read and contribute to the project's source code](https://gitlab.com/veloren/veloren)
* [GitHub (Mirror) — Read the project's source code in the backup repository](https://github.com/veloren/veloren)

*It has unfortunately come to our attention that Veloren's assets, code, and imagery have, on occasion, been used to
promote malicious scams such as 'pump-and-dump' schemes, often involving cryptocurrency or NFTs. The core development
team is clear: Veloren is not, and will never be, a for-profit project, nor do we wish to be associated with regressive,
socially harmful technology like cryptocurrency.*
